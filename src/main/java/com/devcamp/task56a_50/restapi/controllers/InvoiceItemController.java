package com.devcamp.task56a_50.restapi.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task56a_50.restapi.InvoiceItem;

@RestController
public class InvoiceItemController {
    @CrossOrigin
    @GetMapping("/invoices")
    public ArrayList<InvoiceItem> getListInvoiceItem(){
        InvoiceItem hoaDon1 = new InvoiceItem("HD1", "TV", 2, 780000.0);
        InvoiceItem hoaDon2 = new InvoiceItem("HD2", "Quạt", 6, 800000.0);
        InvoiceItem hoaDon3 = new InvoiceItem("HD3", "Tủ Lạnh", 1, 1000000.0);

        System.out.println(hoaDon1);
        System.out.println(hoaDon2);
        System.out.println(hoaDon3);

        ArrayList<InvoiceItem> invoiceItems = new ArrayList<>();

        invoiceItems.add(hoaDon1);
        invoiceItems.add(hoaDon2);
        invoiceItems.add(hoaDon3);

        return invoiceItems;
    }
}
